package com.yfunc.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;

import com.yfunc.common.command.CommandBus;
import com.yfunc.common.command.CommandHandler;
import com.yfunc.common.command.DefaultCommandBus;
import com.yfunc.config.registry.CommandConfigRegistry;

@Configuration
public class CommandConfig {
	@Bean
	public CommandBus commandBus(){
		return new DefaultCommandBus();
	}
	
	@Bean
	@Lazy(false)
	public CommandConfigRegistry eventConfigRegistry(CommandBus commandBus, CommandHandler[] handlers){
		return new CommandConfigRegistry(commandBus, handlers);
	}
	

}
