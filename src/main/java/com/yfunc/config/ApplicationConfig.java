package com.yfunc.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.yfunc.application.UserApplicationService;
import com.yfunc.domain.user.UserRepository;

@Configuration
public class ApplicationConfig {
	
	@Bean
	public UserApplicationService userApplicationService(UserRepository userRepository){
		return new UserApplicationService(userRepository);
	}
	
}
