package com.yfunc.config.registry;

import java.util.Map;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

import com.yfunc.common.command.CommandBus;
import com.yfunc.common.command.CommandHandler;
import com.yfunc.common.command.annotations.Command;

public class CommandConfigRegistry implements ApplicationContextAware {
	
	private CommandBus commandBus;

	public CommandConfigRegistry(CommandBus commandBus, CommandHandler[] handlers) {
		this.commandBus = commandBus;
		for (CommandHandler handler : handlers) {
			this.commandBus.register(handler);
		}
	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext)
			throws BeansException {
		Map<String, Object> annotationHandlerNap = applicationContext.getBeansWithAnnotation(Command.class);
		for (Object handler : annotationHandlerNap.values()) {
			this.commandBus.register(handler);
		}

	}

}
