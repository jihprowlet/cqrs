package com.yfunc.config.registry;

import java.util.Map;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

import com.yfunc.common.event.EventBus;
import com.yfunc.common.event.EventExecutor;
import com.yfunc.common.event.EventHandler;
import com.yfunc.common.event.annotations.Handler;

public class EventConfigRegistry implements ApplicationContextAware {

	public EventConfigRegistry(EventExecutor executor, EventHandler[] handlers) {
		EventBus.setEventExecutor(executor);
		for (EventHandler handler : handlers) {
			EventBus.register(handler);
		}
	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext)
			throws BeansException {
		Map<String, Object> annotationHandlerNap = applicationContext.getBeansWithAnnotation(Handler.class);
		for (Object handler : annotationHandlerNap.values()) {
			EventBus.register(handler);
		}

	}

}
