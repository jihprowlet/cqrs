package com.yfunc.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;

import com.yfunc.common.event.EventExecutor;
import com.yfunc.common.event.EventHandler;
import com.yfunc.common.event.EventStore;
import com.yfunc.common.event.executor.SimpleEventExecutor;
import com.yfunc.common.event.handlers.EventStroeHandler;
import com.yfunc.common.event.store.DefaultEventStore;
import com.yfunc.common.persistence.Session;
import com.yfunc.config.registry.EventConfigRegistry;

@Configuration
public class EventConfig {

	@Bean
	@Lazy(false)
	public EventConfigRegistry eventConfigRegistry(EventExecutor executor, EventHandler[] handlers){
		return new EventConfigRegistry(executor, handlers);
	}
	
	
	@Bean
	public EventStore eventStore(Session query){
		return new DefaultEventStore(query);
	}
	
	@Bean
	public EventExecutor eventExecutor(){
//		return new AsynchronousEventExecutor();
		return new SimpleEventExecutor();
	}
	
	@Bean
	public EventStroeHandler eventStroeHandler(EventStore eventStore){
		return new EventStroeHandler(eventStore);
	}

}
