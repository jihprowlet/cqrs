package com.yfunc.common.util;

import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Set;


public class Reflections {
	
	public static Set<Class> getAllInterfaces(Class clazz) {
		if (clazz.isInterface()) {
			return Collections.singleton(clazz);
		}
		Set<Class> interfaces = new LinkedHashSet<Class>();
		while (clazz != null) {
			Class<?>[] ifcs = clazz.getInterfaces();
			for (Class<?> ifc : ifcs) {
				interfaces.addAll(getAllInterfaces(ifc));
			}
			clazz = clazz.getSuperclass();
		}
		return interfaces;
	}
	
	
}
