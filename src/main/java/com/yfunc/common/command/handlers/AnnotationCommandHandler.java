package com.yfunc.common.command.handlers;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

import com.yfunc.common.command.Command;
import com.yfunc.common.event.EventException;

@SuppressWarnings("rawtypes")
public class AnnotationCommandHandler extends AbstractCommandHandler {


	private Map<Class, Method> handlers;
	private Object handlerObject;

	public AnnotationCommandHandler(Object object) {
		this.handlers = new HashMap<Class, Method>();
		this.handlerObject = object;
		addEventHandlerMethods(object);
	}

	private void addEventHandlerMethods(Object object) {

		if (!object.getClass().isAnnotationPresent(com.yfunc.common.command.annotations.Command.class)) {
			throw new EventException(
					"{0} Not Implements com.yfunc.common.command.annotations.Command Annotation",
					this.handlerObject.getClass().getName());
		}

		for (Method method : object.getClass().getMethods()) {
			if (isEventHandleMethod(method)) {
				handlers.put(method.getParameterTypes()[0], method);
			}
		}
	}

	private boolean isEventHandleMethod(Method method) {
		if (!method.isAnnotationPresent(com.yfunc.common.command.annotations.Command.class)) {
			return false;
		}
		Class<?>[] types = method.getParameterTypes();
		if (types == null || types.length == 0) {
			return false;
		}
		return isInterface(types[0], Command.class);
	}

	private boolean isInterface(Class<?> clazz, Class<?> szInterface) {
		for (Class<?> interfac : clazz.getInterfaces()) {
			if (interfac.equals(szInterface)) {
				return true;
			}
		}
		if (clazz != null && clazz.getSuperclass() != null) {
			return isInterface(clazz.getSuperclass(), szInterface);
		}
		return false;
	}
	
	@Override
	public Class<?>[] supportedCommands() {
		return handlers.keySet().toArray(new Class[0]);
	}

	@Override
	public  Command execute( Command command) {

		if (handlers.containsKey(command.getClass())) {
			Method method = handlers.get(command.getClass());
			try {
				Object returnCommand = method.invoke(this.handlerObject, command);
				return (Command) returnCommand;
			} catch (Exception e) {
				throw new EventException("{0} Not Implements Method: {1}({2})",
						this.handlerObject.getClass().getName(),
						method.getName(), command.getClass().getName());
			}
		} 
		return null;
	}


}
