package com.yfunc.common.command;


public interface CommandHandler {
	
	public Command execute(Command command);
	
	public boolean isSupported(Command command);

}
