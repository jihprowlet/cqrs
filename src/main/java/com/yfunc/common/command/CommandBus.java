package com.yfunc.common.command;


public interface CommandBus {
	
	public Command execute(Command command);
	
	public void register(CommandHandler handler);
	
	public void register(Object handler);
}
