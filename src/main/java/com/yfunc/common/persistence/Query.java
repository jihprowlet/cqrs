package com.yfunc.common.persistence;

import java.util.List;

public interface Query {
	
	public Query first(long firstResult);
	
	public Query max(long maxResult);
	
	public Query and(String propertyName , Object value);
	
	public Query and(String propertyName , Object value , String expression); 
	
	public Query like(String propertyName , String value); 

	public Query orderAsc(String propertyName);
	
	public Query orderDesc(String propertyName);
	
	public Query id(Object pk);
	
	public Long count();
	
	public <T> List<T> list();
	
	public <T> T uniqueResult();
	
	public <T> T get();

}
