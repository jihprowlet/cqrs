package com.yfunc.common.persistence;

import java.util.List;

public interface Session {
	
	int insert(Object entity);

	int update(Object entity);

	int delete(Object entity);

	int delete(Class<?> entityClass, Object pk);
	
	<T> T load(Class<T> entityClass, Object pk);

	<T> List<T> loadAll(Class<T> entityClass);
	
	<T> T selectOne(String statement);

	<T> T selectOne(String statement, Object parameter);

	<E> List<E> selectList(String statement);

	<E> List<E> selectList(String statement, Object parameter);
	
	<T> List<T> query(Class<T> entityClass , String ql , Object... anArguments);
	
	<T> Query createQuery(Class<T> entityClass);
	
}
