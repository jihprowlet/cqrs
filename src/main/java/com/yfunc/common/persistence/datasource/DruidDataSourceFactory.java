package com.yfunc.common.persistence.datasource;

import java.util.Properties;

import javax.sql.DataSource;

import org.apache.ibatis.datasource.DataSourceFactory;

public class DruidDataSourceFactory implements DataSourceFactory {

	private DataSource dataSource;

	public DruidDataSourceFactory() {
		// this.dataSource = new DruidDataSource();
	}

	@Override
	public void setProperties(Properties properties) {

		try {
			this.dataSource = com.alibaba.druid.pool.DruidDataSourceFactory
					.createDataSource(properties);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public DataSource getDataSource() {
		return dataSource;
	}

}
