package com.yfunc.common.persistence.ibatis;

import com.yfunc.common.persistence.ibatis.CommandType.Type;

public interface Handler {
	
	Command execute(Command command , Type commandType);
	
	boolean isSupported(Type commandType);
}
