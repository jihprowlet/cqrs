package com.yfunc.common.persistence.ibatis.handlers;

import java.util.Map;

import com.yfunc.common.persistence.ibatis.Command;
import com.yfunc.common.persistence.ibatis.CommandType.Type;

public class DeleteByIdHandler extends AbstractHandler {

	@Override
	public Command execute(Command command, Type commandType) {
		Map params = command.getParameter();
		Class entityClass = (Class) params.get("0");
		Object pk = params.get("1");
		String sql = commandType.toSQL(entityClass);
		return new Command(command.newMappedStatementBuilder(sql, entityClass).build(), pk);
	}

	@Override
	public Type[] supportedTypes() {
		return new Type[] {Type.DELETEBYID };
	}

}
