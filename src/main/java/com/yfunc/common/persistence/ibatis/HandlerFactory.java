package com.yfunc.common.persistence.ibatis;

import java.util.ArrayList;
import java.util.List;

import com.yfunc.common.persistence.ibatis.handlers.DeleteByIdHandler;
import com.yfunc.common.persistence.ibatis.handlers.FindAllHandler;
import com.yfunc.common.persistence.ibatis.handlers.FindByIdHandler;
import com.yfunc.common.persistence.ibatis.handlers.UpdateHandler;

public class HandlerFactory {
	
	private List<Handler> handlers;
	
	public HandlerFactory(){
		this.handlers = new ArrayList<Handler>();
		this.handlers.add(new UpdateHandler());
		this.handlers.add(new DeleteByIdHandler());
		this.handlers.add(new FindByIdHandler());
		this.handlers.add(new FindAllHandler());
	}
	
	public  List<Handler> getHandlers(){
		return this.handlers;
	}
	
	public void addHandler(Handler handler){
		this.handlers.add(handler);
	}
}
