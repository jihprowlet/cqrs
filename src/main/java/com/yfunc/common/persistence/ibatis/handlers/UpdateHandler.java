package com.yfunc.common.persistence.ibatis.handlers;

import com.yfunc.common.persistence.ibatis.Command;
import com.yfunc.common.persistence.ibatis.CommandType.Type;

public class UpdateHandler extends AbstractHandler {

	@Override
	public Command execute(Command command, Type commandType) {
		Object parameter = command.getParameter();
		String sql = commandType.toSQL(parameter);
		return new Command(command.newMappedStatementBuilder(sql , parameter.getClass()).build(), parameter);
	}

	@Override
	public Type[] supportedTypes() {
		return new Type[] { Type.INSERT, Type.UPDATE, Type.DELETE };
	}

}
