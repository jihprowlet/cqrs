package com.yfunc.common.persistence.ibatis.handlers;

import java.util.Collections;
import java.util.Map;

import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.mapping.ResultMap;

import com.yfunc.common.persistence.ibatis.Command;
import com.yfunc.common.persistence.ibatis.CommandType;
import com.yfunc.common.persistence.ibatis.CommandType.Type;

public class FindByIdHandler extends AbstractHandler {

	@Override
	public Command execute(Command command, Type commandType) {
		Map params = command.getParameter();
		Class entityClass = (Class) params.get("0");
		Object pk = params.get("1");
		String sql = commandType.toSQL(entityClass);
		//set result maps
		MappedStatement.Builder buidler =  command.newMappedStatementBuilder(sql , entityClass);
		ResultMap resultMap = command.newResultMap(entityClass);
		buidler.resultMaps(Collections.singletonList(resultMap));
		return new Command(buidler.build(), pk);
	}

	@Override
	public Type[] supportedTypes() {
		return new Type[] { Type.FINDBYID};
	}

}
