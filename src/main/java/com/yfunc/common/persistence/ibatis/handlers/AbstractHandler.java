package com.yfunc.common.persistence.ibatis.handlers;

import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.mapping.SqlSource;
import org.apache.ibatis.mapping.MappedStatement.Builder;

import com.yfunc.common.persistence.ibatis.Handler;
import com.yfunc.common.persistence.ibatis.CommandType.Type;

public abstract class AbstractHandler implements Handler {

	@Override
	public boolean isSupported(Type commandType) {
		if (commandType == null || supportedTypes() == null) {
			return false;
		}

		for (Type supportedType : supportedTypes()) {
			if (supportedType != null && supportedType.equals(commandType)) {
				return true;
			}
		}
		return false;
	}

	public Type[] supportedTypes() {
		return null;
	}

	// see: MapperBuilderAssistant
	protected Builder createMappedStatementBuilder(MappedStatement ms,
			SqlSource sqlSource) {
		Builder builder = new MappedStatement.Builder(ms.getConfiguration(),ms.getId(), sqlSource, ms.getSqlCommandType());

		builder.resource(ms.getResource());
		builder.fetchSize(ms.getFetchSize());
		builder.statementType(ms.getStatementType());
		builder.keyGenerator(ms.getKeyGenerator());
		// builder.keyProperty(ms.getk);

		// setStatementTimeout()
		builder.timeout(ms.getTimeout());

		// setStatementResultMap()
		builder.parameterMap(ms.getParameterMap());
		builder.resultMaps(ms.getResultMaps());
		builder.resultSetType(ms.getResultSetType());

		// setStatementCache()
		builder.cache(ms.getCache());
		builder.flushCacheRequired(ms.isFlushCacheRequired());
		builder.useCache(ms.isUseCache());
		return builder;
	}
}
