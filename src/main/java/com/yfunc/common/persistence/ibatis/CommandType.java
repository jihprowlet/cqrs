package com.yfunc.common.persistence.ibatis;



public class CommandType {

	public static boolean isHandlerType(String id) {
		return id != null && id.startsWith(EntityMapper.class.getName().toLowerCase());
	}
	
	public static Type getType(String id) {
		for ( Type type : Type.values()) {
			if( type.toKey().equals(id.toLowerCase())) {
				return type;
			}
		}
		return null;
	}
	
	public enum Type{
		INSERT {
			@Override
			public String toSQL(Class entityClass) {
				return getEntityInfo(entityClass).insertSQL();
			}
		},
		UPDATE {
			@Override
			public String toSQL(Class entityClass) {
				return getEntityInfo(entityClass).updateSQL();
			}
		},
		DELETE {
			@Override
			public String toSQL(Class entityClass) {
				return getEntityInfo(entityClass).deleteSQL();
			}
		},
		DELETEBYID {
			@Override
			public String toSQL(Class entityClass) {
				return getEntityInfo(entityClass).deleteSQL();
			}
		},
		FINDBYID {
			@Override
			public String toSQL(Class entityClass) {
				return getEntityInfo(entityClass).fingByIdSQL();
			}
		},
		FINDALL {
			@Override
			public String toSQL(Class entityClass) {
				return getEntityInfo(entityClass).fingAllSQL();
			}
		};

		public String toSQL(Object parameter){
			return toSQL(parameter.getClass());
		}
		
		public String toKey(){
			String key = EntityMapper.class.getName() + "." + this.name();
			return key.toLowerCase();
		}
		
		public boolean isType(String key){
			return key != null && toKey().equals(key.toLowerCase());
		}
		
		public abstract String toSQL(Class entityClass);
		
		//no cache
		protected EntityInfo getEntityInfo(Class entitClass) {
			return new EntityInfo(entitClass);
		}
	}
}
