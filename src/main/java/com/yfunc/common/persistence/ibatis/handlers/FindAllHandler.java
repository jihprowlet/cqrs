package com.yfunc.common.persistence.ibatis.handlers;

import java.util.Collections;

import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.mapping.ResultMap;

import com.yfunc.common.persistence.ibatis.Command;
import com.yfunc.common.persistence.ibatis.CommandType;
import com.yfunc.common.persistence.ibatis.CommandType.Type;

public class FindAllHandler extends AbstractHandler {

	@Override
	public Command execute(Command command, Type commandType) {
		Class<?> entityClass = command.getParameter();
		String sql = commandType.toSQL(entityClass);
		//set result maps
		MappedStatement.Builder buidler =  command.newMappedStatementBuilder(sql , entityClass);
		ResultMap resultMap = command.newResultMap(entityClass);
		buidler.resultMaps(Collections.singletonList(resultMap));
		return new Command(buidler.build(), command.getParameter());
	}

	@Override
	public Type[] supportedTypes() {
		return new Type[] { Type.FINDALL};
	}

}
