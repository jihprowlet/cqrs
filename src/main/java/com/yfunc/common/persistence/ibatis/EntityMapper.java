package com.yfunc.common.persistence.ibatis;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

public interface EntityMapper {
	
	@Insert("")
	int insert(Object entity);
	
	@Update("")
	int update(Object entity);
	
	@Delete("")
	int delete(Object entity);
	
	@Delete("")
	int deleteById(Class<?> entityClass , Object pk);
	
	@Select("")
	Object findById(Class<?> entityClass , Object pk);

	@Select("")
	List findAll(Class<?> entityClass);
}
