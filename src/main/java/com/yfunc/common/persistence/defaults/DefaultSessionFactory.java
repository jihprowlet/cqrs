package com.yfunc.common.persistence.defaults;

import javax.sql.DataSource;

import com.yfunc.common.persistence.Session;
import com.yfunc.common.persistence.SessionFactory;

public class DefaultSessionFactory implements SessionFactory {
	private Settings settings;

	public DefaultSessionFactory(Settings settings) {
		this.settings = settings;
	}

	@Override
	public DataSource getDataSource() {
		return this.settings.getConfiguration().getEnvironment().getDataSource();
	}

	@Override
	public Session createSession() {
		return new DefaultSession(settings.getSqlSessionFactory() , this);
	}

}
