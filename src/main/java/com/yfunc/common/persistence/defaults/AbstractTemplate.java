package com.yfunc.common.persistence.defaults;

import org.apache.ibatis.session.SqlSessionFactory;

public class AbstractTemplate {

	private SqlSessionFactory sqlSessionFactory;
	private SqlSessionTemplate template;

	public AbstractTemplate(SqlSessionFactory sqlSessionFactory) {
		this.sqlSessionFactory = sqlSessionFactory;
		this.template = new SqlSessionTemplate(sqlSessionFactory);
	}

	protected SqlSessionFactory getSqlSessionFactory() {
		return sqlSessionFactory;
	}

	protected SqlSessionTemplate getTemplate() {
		return template;
	}

}
