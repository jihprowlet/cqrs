package com.yfunc.common.persistence;

import javax.sql.DataSource;

public interface SessionFactory {
	
	DataSource getDataSource();

	Session createSession();
	
}