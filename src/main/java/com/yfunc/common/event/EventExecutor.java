package com.yfunc.common.event;

public interface EventExecutor {
	
	void publish(Event event);
	
	void register(EventHandler handler);
	
	void register(Object handler);
	
}
