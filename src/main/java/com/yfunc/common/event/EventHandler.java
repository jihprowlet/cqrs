package com.yfunc.common.event;


public interface EventHandler {
	
	public void handle(Event event);
	
	public boolean isSupported(Event event);
}