package com.yfunc.common.event.handlers;

import com.yfunc.common.event.Event;
import com.yfunc.common.event.EventHandler;
import com.yfunc.common.event.EventStore;

public class EventStroeHandler implements EventHandler {
	
	private EventStore eventStore;
	
	public EventStroeHandler(EventStore eventStore){
		this.eventStore = eventStore;
	}
	
	@Override
	public void handle(Event event) {		
		this.eventStore.append(event);
	}

	@Override
	public boolean isSupported(Event event) {
		return true;
	}

	

}
