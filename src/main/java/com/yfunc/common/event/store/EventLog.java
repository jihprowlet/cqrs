package com.yfunc.common.event.store;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class EventLog {
	@Id
	@GeneratedValue
	private long id;
	@Column
	private String data;
	@Column
	private String type;
	@Column
	private long created;
	@Column
	private long version;
	
	
	public EventLog(){}
	
	public EventLog( String data, String type, long created , long version) {
		this.data = data;
		this.type = type;
		this.created = created;
		this.version = version;
	}
	
	public long getId() {
		return id;
	}
	public String getData() {
		return data;
	}
	public long getCreated() {
		return created;
	}
	public long getVersion() {
		return version;
	}
	public String getType() {
		return type;
	}
}
