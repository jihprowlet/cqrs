package com.yfunc.common.event;

import java.util.List;



public interface EventStore {
	
	void append( Event event);
	
	<T extends Event> T findLatestEvent(Class<T> eventType);
	
	<T extends Event> T findEvent(Class<T> eventType , long version);
	
	<T extends Event> List<T> findEvents(Class<T> eventType , long version);
	
	<T extends Event> List<T> findEventRange(Class<T> eventType , long beginTimemillis ,long endTimemillis);
	
	List findEventRange(long beginTimemillis ,long endTimemillis);
}
