package com.yfunc.common.event;

public class EventBus {

	private static EventExecutor executor;
	private EventBus() {
	}
	
	private static EventExecutor getEventExecutor() {
		return executor;
	}
	
	public static void register(EventHandler handler) {
		getEventExecutor().register(handler);
	}
	
	public static void register(Object handler){
		getEventExecutor().register(handler);
	}

	public static void publish(Event event) {
		getEventExecutor().publish(event);
	}
	
	public static void setEventExecutor(EventExecutor executor){
		EventBus.executor = executor;
	}
}
