package com.yfunc.common.event;

import com.yfunc.common.util.Strings;

public class EventException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	public EventException(String message) {
		super(message);
	}

	public EventException(Throwable cause) {
		super(cause);
	}

	public EventException(String message, String... params) {
		super(Strings.value(message, params));
	}

}
