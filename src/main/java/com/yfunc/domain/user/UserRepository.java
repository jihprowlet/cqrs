package com.yfunc.domain.user;

public interface UserRepository { 
	
	int insert(User user);
	
	int update(User user);
	
	int remove(User user);
	
	User findById(long userId);
}
