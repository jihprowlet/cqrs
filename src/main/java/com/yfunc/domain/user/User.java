package com.yfunc.domain.user;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity(name = "users")
public class User {

	@Id
	@GeneratedValue
	private long userId;
	private String username;
	private String password;
	private String mail;

	public User() {
	}
	public User(String username, String password, String mail) {
		super();
		this.username = username;
		this.password = password;
		this.mail = mail;
	}

	public long getUserId() {
		return userId;
	}
	public String getUsername() {
		return username;
	}
	public String getPassword() {
		return password;
	}
	public String getMail() {
		return mail;
	}
	
	public void changeUserName(String newUserName){
		this.username  = newUserName;
	}
}
