package com.yfunc.adapter.ibatis;

import com.yfunc.common.persistence.Session;
import com.yfunc.domain.user.User;
import com.yfunc.domain.user.UserRepository;

public class IbatisUserRepository implements UserRepository {
	
	private Session repository;
	
	public IbatisUserRepository(Session repository){
		this.repository = repository;
	}
	
	@Override
	public int insert(User user) {
		return this.repository.insert(user);
	}

	@Override
	public int update(User user) {
		return this.repository.update(user);
	}

	@Override
	public int remove(User user) {
		return this.repository.delete(user);
	}

	@Override
	public User findById(long userId) {
		return this.repository.load(User.class, userId);
	}
	
	
}
