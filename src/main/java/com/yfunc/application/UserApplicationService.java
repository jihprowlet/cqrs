package com.yfunc.application;

import com.yfunc.application.events.ChangedUserNameEvent;
import com.yfunc.application.events.CreateUserEvent;
import com.yfunc.common.event.annotations.Handler;
import com.yfunc.domain.user.User;
import com.yfunc.domain.user.UserRepository;

@Handler
public class UserApplicationService {
	
	private UserRepository userRepository;
	
	public UserApplicationService(UserRepository userRepository){
		this.userRepository = userRepository;
	}
	
	@Handler
	public void changedUserName(ChangedUserNameEvent event ){
		User user = this.userRepository.findById(event.getUserId());
		user.changeUserName(event.getNewUserName());
		this.userRepository.update(user);
	}
	
	@Handler
	public void createUser(CreateUserEvent event ){
		User user = new User(event.getUsername(), event.getPassword(), event.getMail());
		this.userRepository.insert(user);
	}

}
