package com.yfunc.application.events;

import com.yfunc.common.event.Event;

public class RemoveedUserEvent implements Event {
	
	private long userId;
	
	public RemoveedUserEvent(long userId) {
		this.userId = userId;
	}
	
	public long getUserId() {
		return userId;
	}

}
