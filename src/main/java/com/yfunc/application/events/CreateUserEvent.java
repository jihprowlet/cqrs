package com.yfunc.application.events;

import com.yfunc.common.event.Event;
import com.yfunc.common.event.annotations.Storage;

public class CreateUserEvent implements Event {
	@Storage
	private String username;
	@Storage
	private String password;
	@Storage
	private String mail;
	
	public CreateUserEvent(String username, String password, String mail) {
		super();
		this.username = username;
		this.password = password;
		this.mail = mail;
	}
	
	
	public String getUsername() {
		return username;
	}
	public String getPassword() {
		return password;
	}
	public String getMail() {
		return mail;
	}
	
	

}
