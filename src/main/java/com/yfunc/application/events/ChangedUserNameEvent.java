package com.yfunc.application.events;

import com.yfunc.common.event.Event;

public class ChangedUserNameEvent implements Event {
	
	private long userId;
	private String newUserName;
	
	public ChangedUserNameEvent(long userId, String newUserName) {
		this.userId = userId;
		this.newUserName = newUserName;
	}
	
	public long getUserId() {
		return userId;
	}
	public String getNewUserName() {
		return newUserName;
	}
	
	
	

}
