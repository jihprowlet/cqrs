package com.yfunc.orm;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.yfunc.common.persistence.Query;
import com.yfunc.common.persistence.Session;
import com.yfunc.config.PersistenceConfig;
import com.yfunc.domain.user.User;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration( classes = { PersistenceConfig.class })
public class OrmTest {
	
	
	@Autowired
	private Session query;

//	@Test
	public void test() {

	
		for( int i = 0 ; i < 100 ; i++) {
			User user = new User("aa" + i, "bb" + i, "cc@mail.com");
			query.insert(user);
		}
	}
	
//	@Test
	public void testQuery(){
		
		for( int i = 0 ; i < 100 ; i++) {
			query.selectList("User.findAll");
		}
	}
	
	@Test
	public void testJDBC(){
		Query q = query.createQuery(User.class).and("username" , "aa0").and("password", "bb0");
		List<User> users = q.list();
		
		System.out.println(q.count());
	
		for( User user : users) {
			System.out.println(user.getUsername());
		}
		
		User user = query.createQuery(User.class).id(1550).get();
		
		System.out.println(user.getUsername());
	}
	
	@Test
	public void testQue(){
		List<User> users =  query.query(User.class , "Select * FROM users");
		System.out.println(users.size());
	}

}
