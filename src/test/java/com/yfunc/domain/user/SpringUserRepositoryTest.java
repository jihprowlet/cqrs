package com.yfunc.domain.user;

import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.yfunc.config.PersistenceConfig;

public class SpringUserRepositoryTest {

	@Test
	public void test() {
		ApplicationContext context = new AnnotationConfigApplicationContext(PersistenceConfig.class);
		UserRepository  repository = context.getBean(UserRepository.class);
		
		for( int i = 0 ; i < 1000 ; i++) {
			User user = new User("aa" + i, "bb" + i, "cc@mail.com");
			repository.insert(user);
		}
	
	}

}
