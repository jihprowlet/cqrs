package com.yfunc.application;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.yfunc.application.events.CreateUserEvent;
import com.yfunc.common.event.EventBus;
import com.yfunc.config.ApplicationConfig;
import com.yfunc.config.EventConfig;
import com.yfunc.config.PersistenceConfig;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration( classes = { PersistenceConfig.class , EventConfig.class , ApplicationConfig.class})
public class UserApplicationServiceTest {
	
	@Test
	public void test() {
		CreateUserEvent create = new CreateUserEvent("huangjiu" , "123" , "cc@mi.com");
		EventBus.publish(create);
	}

}
