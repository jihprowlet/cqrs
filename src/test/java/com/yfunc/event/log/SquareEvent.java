package com.yfunc.event.log;

import com.yfunc.common.event.Event;
import com.yfunc.common.event.annotations.Storage;

public class SquareEvent implements Event {

	@Storage
	private int value;
	
	public SquareEvent(){}
	
	public SquareEvent( int value) {
		this.value = value;
	}
	
	public int getValue(){
		return this.value;
	}


}
