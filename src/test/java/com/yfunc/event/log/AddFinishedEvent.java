package com.yfunc.event.log;

import com.yfunc.common.event.Event;


public class AddFinishedEvent implements Event {

	private int value;
	
	public AddFinishedEvent(){}
	
	public AddFinishedEvent(int value) {
		this.value = value;
	}
	
	public int getValue(){
		return this.value;
	}


}
