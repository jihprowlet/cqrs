package com.yfunc.event.log;

import com.yfunc.common.event.Event;

public class SquareFinishedEvent implements Event {

	private int value;
	
	public SquareFinishedEvent(int value) {
		this.value = value;
	}
	
	public int getValue(){
		return this.value;
	}


}
