package com.yfunc.event;

import com.yfunc.common.event.EventBus;
import com.yfunc.common.event.annotations.Handler;
import com.yfunc.event.log.SquareEvent;
import com.yfunc.event.log.SquareFinishedEvent;

@Handler
public class AnnotationTest {
	
	@Handler
	public void on(SquareEvent event ){
		System.out.println("squareEvent handler ...");
		EventBus.publish( new SquareFinishedEvent( 1) );
	}
	
	@Handler
	public void on(SquareFinishedEvent event ){
		System.out.println("finished squareEvent handler ...");
	}
	
	
	public static void main(String[] args){
		EventBus.register(new AnnotationTest());
		EventBus.publish(new SquareEvent(1));
		
		
	}
}
